ChatGPT个人小助手应用搭建

# ChatGPT在线个人小助手应用搭建
![效果图](./doc/WeChat%20Image_20230215135721.png)
## 在线体验
[点我在线体验](http://124.220.38.75/)

```
因为openAI账户申请后会默认有18美元的账户,
openAI每次调用大概会花掉0.01美元,
所以为了防止恶意刷api,无意义聊天,
页面做了密码限制,如果密码不对,是不会启用openAI智能回复的.
```

```
代码此文就不讲解了,源码面前,了无秘密.
后端:https://gitee.com/fengkuangdejava/chatGpt.git
前端:https://gitee.com/fengkuangdejava/chat-view.git
```

## 快速部署
### 环境准备(cenos云服务一台)

```sh
	#cenos云服务一台 其它linux系统也行 只是此处命令使用了yum,其它系统可能不一样
	yum install -y unzip nginx java-1.8.0-openjdk* dos2unix
	service nginx start
```


### 部署后端
```sh
# 安装后端服务
wget http://124.220.38.75:8080/monkey-chat-0.0.1-SNAPSHOT.tar.gz
tar -zxvf monkey-chat-0.0.1-SNAPSHOT.tar.gz
# 修改monkey-chat/application-dev.yml配置文件中的token为自己的token,如果没有token,下面会说如果获取token
dos2unix */*.sh&&dos2unix */*/*.sh
sh monkey-chat/deploy-tar.sh
#查看项目启动日志,部署成功
tail -f /var/logs/ai-chat/dev/info.log
#重启 
nohup sh /opt/java/chat-gpt-java/monkey-chat/bin/tar-service.sh restart >/dev/null 2>&1 &

#其它说明
#项目日志目录
/var/logs/ai-chat/dev/
#项目配置目录
/opt/java/chat-gpt-java/monkey-chat/conf
#项目启动文件
/opt/java/chat-gpt-java/monkey-chat/bin
#项目启动命令
#启动
nohup sh /opt/java/chat-gpt-java/monkey-chat/bin/tar-service.sh start >/dev/null 2>&1 &
#停止
nohup sh /opt/java/chat-gpt-java/monkey-chat/bin/tar-service.sh stop >/dev/null 2>&1 &
#重启
nohup sh /opt/java/chat-gpt-java/monkey-chat/bin/tar-service.sh restart >/dev/null 2>&1 &
```
### 部署前端

```sh
# 获取前端资源包并部署
mkdir -p /opt/chat-web/chat-view
wget -P /opt/chat-web/chat-view/ http://124.220.38.75:8080/dist.zip
unzip -d /opt/chat-web/chat-view/ /opt/chat-web/chat-view/dist.zip

# !!! 以下看情况执行!!!
## 后端服务没换端口的情况下 替换ip 把文件中的公网ip 替换成你自己的后台服务的域名或者ip
cd /opt/chat-web/chat-view&&sed -i "s/124.220.38.75/公网ip/g" `find . -type f -name "*.js"` >/dev/null 2>&1
## 后端服务换了端口的情况下 替换ip+端口 把文件中的公网ip 替换成你自己的后台服务的域名或者ip+端口
cd /opt/chat-web/chat-view&&sed -i "s/124.220.38.75:8079/公网ip:端口/g" `find . -type f -name "*.js"` >/dev/null 2>&1
# 开启nginx 此处已默认的nginx配置文件目录为例,有的nginx配置文件目录不在/etc/nginx/conf.d
wget -P /etc/nginx/conf.d/ http://124.220.38.75:8080/8080.conf
# 默认是8080 如果冲突 请修改/etc/nginx/conf.d/8080.conf文件的内部配置的端口和当前文件名
service nginx reload

# 检查公网是否开通端口 8097,8080.
# 然后尝试访问
http://你的公网ip:8080/
```
## Token获取
```
token获取见:https://github.com/fengkuangdejava/chat-gpt
```
[点我直达](https://github.com/fengkuangdejava/chat-gpt)