    #!/bin/sh
    # --------------------声明 start--------------------------------------
    # tar包形式部署时专用
    # 使用方式 sh deploy-tar.sh
    # ${project.basedir}为springboot项目Application启动类所在模块的根目录
    # 该shell脚本文件在项目的的存放位置-${project.basedir}/deploy/sh
    # 在该位置下的../../install/assembly-tar.xml会扫描该文件并将该文件加入部署包
    # --------------------声明 end----------------------------------------
    #
    #----------------------- 不同项目需要修改的部分 start--------------------
    # 服务名称
    # springboot项目请修改为Application启动类所在模块的artifactId
    SERVICE_NAME="monkey-chat"
    # 部署目录
    # tar包形式默认部署在/opt目录且已目标客户或者业务线区分如/wy,/shining
    DEPLOY_PATH="/opt/java/chat-gpt-java"
    #----------------------- 不同项目需要修改的部分 end--------------------
    #
    ##
    echo "服务名称:$SERVICE_NAME"
    serviceBasePath="$DEPLOY_PATH/$SERVICE_NAME"
    currentPath=$(pwd)
    # shellcheck disable=SC1009
    if  [ "$currentPath" = "$serviceBasePath" ] ;then
      echo "当前目录就是部署目录,直接启动"
    else
      if  [ ! -d "$DEPLOY_PATH/" ] ; then
        mkdir -p "$DEPLOY_PATH/"
      fi
      if  [ ! -d "$serviceBasePath" ] ; then
        echo "目录不存在,开始新建：$serviceBasePath"
        mkdir -p "$serviceBasePath"
      fi
      CLEAR_CMD="rm -rf $serviceBasePath/*"
      CP_CMD="cp -rf $currentPath/* $serviceBasePath/"
      echo "当前目录不是部署目录,开始迁移:"
      echo $CLEAR_CMD
      echo $CP_CMD
      eval $CLEAR_CMD
      eval $CP_CMD
    fi
    DOS2UNIX="dos2unix $DEPLOY_PATH/$SERVICE_NAME/bin/*.sh"
    echo "服务部署完毕,开始脚本编码转换：$DOS2UNIX"
    eval $DOS2UNIX
    START_CMD="nohup sh $DEPLOY_PATH/$SERVICE_NAME/bin/tar-service.sh restart >/dev/null 2>&1 &"
    echo "开始启动:$START_CMD"
    eval $START_CMD
    exit 0
