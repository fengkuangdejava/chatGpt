package com.tiger.chat.config;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
@Slf4j
@ToString
public class ChatProperty implements InitializingBean {
    @Value("${openAi.timeout}")
    private int timeout;
    @Value("${openAi.token}")
    private String token;

    @Value("${openAi.pwd}")
    private String pwd;

    @Value("${openAi.enable}")
    private boolean enable;

    @Override
    public void afterPropertiesSet(){
        log.info("智能聊天配置初始化：{}",this);
        WebSocketServer.initChatProperty(this);
    }
}
