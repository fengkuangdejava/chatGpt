package com.tiger.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@EnableWebSocket
@SpringBootApplication
public class MonkeyChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonkeyChatApplication.class, args);
    }

}
