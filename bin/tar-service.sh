    #!/bin/sh
    # --------------------声明 start--------------------------------------
    # tar包部署后专用启动、停止、重启脚本
    # 使用方式 sh tar-service.sh start, sh tar-service.sh stop ,sh tar-service.sh restart
    # 该shell脚本文件在项目的的存放位置-${project.basedir}/bin
    # ${project.basedir}为springboot项目Application启动类所在模块的根目录
    # 在该文件目录下的../install/assembly-tar.xml会扫描该文件并将该文件加入部署包
    # --------------------声明 end----------------------------------------
    #
    #
    #----------------------- 不同项目需要修改的部分 start--------------------
    # 服务名称
    # springboot项目请修改为Application启动类所在模块的artifactId
    SERVICE_NAME="monkey-chat"
    # 服务部署目录
    DEPLOY_PATH="/opt/java/chat-gpt-java"
    # gc日志文件目录
    # 建议项目中logback.xml配置的目录相同
    GCLogHome="/var/logs/monkey-chat"
    #----------------------- 不同项目需要修改的部分 end---------------------
    #
    ##
    echo "服务名称:$SERVICE_NAME"
    echo "文件所在目录：$DEPLOY_PATH"
    echo "GC日志目录：$GCLogHome"
    GCCommand="-XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -XX:+PrintHeapAtGC     -XX:+PrintReferenceGC -XX:+PrintGCApplicationStoppedTime -XX:+PrintSafepointStatistics     -XX:PrintSafepointStatisticsCount=1 -Xloggc:$GCLogHome/gc-%t.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=14     -XX:GCLogFileSize=100M"
    # shellcheck disable=SC1009
    # shellcheck disable=SC2112
    function startService() {
      # shellcheck disable=SC2061
      FIND_JAR_CMD="find $DEPLOY_PATH/$SERVICE_NAME/ -name '$SERVICE_NAME*.jar'|grep -v $SERVICE_NAME*sources.jar "
      echo "判断$DEPLOY_PATH/$SERVICE_NAME/路径下是否存在执行jar包"
      echo "$FIND_JAR_CMD"
      service_jar_path=$(eval $FIND_JAR_CMD)
      # shellcheck disable=SC2236
      if [ ! $service_jar_path ]; then
        echo "$DEPLOY_PATH/$SERVICE_NAME/下没有找到执行jar包"
      else
        echo "找到jar包,路径为：$service_jar_path"
        if [ ! -n "${MY_JAVA_OPTS}" ]; then
          START_CMD="java -jar $GCCommand $service_jar_path"
          echo "没有找到JVM参数,无参启动:$START_CMD"
          eval $START_CMD
        else
          START_CMD="java ${MY_JAVA_OPTS} -jar $GCCommand $service_jar_path"
          echo "jvm参数：${MY_JAVA_OPTS},开始启动:$START_CMD"
          eval $START_CMD
        fi
      fi
      return 1
    }

    # shellcheck disable=SC2112
    function stopService() {
      # shellcheck disable=SC2009
      service_process=$(ps -ef | grep $SERVICE_NAME | grep -v grep | grep .jar | grep java | awk '{print $2}')
      # shellcheck disable=SC2236
      if [ ! -n "$service_process" ]; then
        echo '没有找到进程'
      else
        echo "进程号为：$service_process"
        kill -9 "$service_process"
      fi
      return 1
    }

    command_line=$1
    # shellcheck disable=SC2236
    if [ ! -n "$command_line" ]; then
      echo "请输入要执行的动作：start stop restart"
    elif [ "$command_line" = "start" ]; then
      echo "开始启动服务$SERVICE_NAME"
      startService
    elif [ "$command_line" = "stop" ]; then
      echo "开始停止服务$SERVICE_NAME"
      stopService
    elif [ "$command_line" = "restart" ]; then
      echo "开始重启服务$SERVICE_NAME"
      stopService
      startService
    fi
    exit 0
